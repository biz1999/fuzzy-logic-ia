import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
from skfuzzy import control as ctrl

#Variaveis de Entrada (Antecedent)
angulo = ctrl.Antecedent(np.arange(0, 361, 1), 'Angulo')

angulo['NLS'] = fuzz.trimf(angulo.universe, [90, 130, 170])
angulo['NBS'] = fuzz.trimf(angulo.universe, [30, 150, 170])
angulo['SALN'] = fuzz.trimf(angulo.universe, [170, 175, 180])
angulo['Z'] = fuzz.trimf(angulo.universe, [180, 180, 180])
angulo['SALP'] = fuzz.trimf(angulo.universe, [180, 185, 190])
angulo['PBS'] = fuzz.trimf(angulo.universe, [190, 210, 330])
angulo['PLS'] = fuzz.trimf(angulo.universe, [190, 230, 270])

velocidade_angulo = ctrl.Antecedent(np.arange(-10, 11, 1), 'Velocidade Angulo')

velocidade_angulo['NEG'] = fuzz.trapmf(velocidade_angulo.universe, [-10, -10, -1, 0])
velocidade_angulo['ZS'] = fuzz.trapmf(velocidade_angulo.universe, [-0.1, 0, 0, 0.1])
velocidade_angulo['POS'] = fuzz.trapmf(velocidade_angulo.universe, [0, 1, 10, 10])


#Variaveis de saída (Consequent)
forca_aplica = ctrl.Consequent(np.arange(-6, 6, 0.2), 'Forca Aplicada')


forca_aplica['NB'] = fuzz.trapmf(forca_aplica.universe, [-6, -6, -4.2, -1.7])
forca_aplica['N'] = fuzz.trapmf(forca_aplica.universe, [-3.6, -1.7, -1.7, 0])
forca_aplica['Z'] = fuzz.trimf(forca_aplica.universe, [-1.7, 0, 1.7])
forca_aplica['P'] = fuzz.trapmf(forca_aplica.universe, [0, 1.7 , 1.7, 3.6])
forca_aplica['PB'] = fuzz.trapmf(forca_aplica.universe, [1.7, 4.2, 6,6])

#Criando as regras
regra = []
regra.append( ctrl.Rule(angulo['NLS'] & velocidade_angulo['POS'], forca_aplica['NB']))
regra.append( ctrl.Rule(angulo['NBS'] & velocidade_angulo['POS'], forca_aplica['Z']))
regra.append( ctrl.Rule(angulo['SALN'] & velocidade_angulo['POS'], forca_aplica['N']))
regra.append( ctrl.Rule(angulo['Z'] & velocidade_angulo['ZS'], forca_aplica['P']))
regra.append( ctrl.Rule(angulo['SALP'] & velocidade_angulo['NEG'], forca_aplica['P']))
regra.append( ctrl.Rule(angulo['PBS'] & velocidade_angulo['NEG'], forca_aplica['Z']))
regra.append( ctrl.Rule(angulo['PLS'] & velocidade_angulo['NEG'], forca_aplica['PB']))


def plotarSaidasSwingUp():
        #Visualizando as variáveis
        angulo.view()
        velocidade_angulo.view()
        forca_aplica.view()
        plt.show()

def calculaForcaSwingUp(valores):
        input_angulo,  input_velocidadeAngular = valores
        CalculoForca = ctrl.ControlSystemSimulation(ctrl.ControlSystem(regra))

        CalculoForca.input['Angulo'] = input_angulo
        CalculoForca.input['Velocidade Angulo'] = input_velocidadeAngular
        CalculoForca.compute()

        forcaAplicadaResultado = CalculoForca.output['Forca Aplicada']

        plotarSimulacao(CalculoForca)

        return forcaAplicadaResultado


def plotarSimulacao(CalculoForca):
    angulo.view(sim=CalculoForca)
    velocidade_angulo.view(sim=CalculoForca)
    forca_aplica.view(sim=CalculoForca)

    plt.show()
