from stabilization import *
from SwingUp import *


def calcularEstabilizar(valores):
    plotarSaidasEstabilizar()
    resultado = calculaForcaEstabilizar(valores)
    return resultado


def calcularSwingUp(valores):
    plotarSaidasSwingUp()
    resultado = calculaForcaSwingUp(valores)
    return resultado

def main():
    print("***Simulação***")
    print("1 - Swing Up")
    print("2 - Estabilizar")

    escolhaSimulacao = 0
    while True:
        escolhaSimulacao = int(input("Escolha uma: "))
        if escolhaSimulacao == 1 or escolhaSimulacao == 2:
            break

    angulo = float(input("\nAngulo: "))
    velocidadeAngular = float(input("Velocidade angular: "))

    if escolhaSimulacao == 1:
        valores = [angulo, velocidadeAngular]
        resultado = calcularSwingUp(valores)
        print(f"Força aplicada para swing up: {resultado}")

    elif escolhaSimulacao == 2:
        posicaoCarrinho = float(input("Posição do carrinho: "))
        velocidadeCarrinho = float(input("Velocidade do carrinho: "))
        valores = [angulo, velocidadeAngular,
                   posicaoCarrinho, velocidadeCarrinho]

        resultado = calcularEstabilizar(valores)
        print(f"Força aplicada para estabilizar: {resultado}")


if __name__ == "__main__":
    main()
