import numpy as np
import skfuzzy as fuzz
from matplotlib import pyplot as plt
from skfuzzy import control as ctrl


#variáveis de entrada
angulo = ctrl.Antecedent(np.arange(-30, 30, 0.5), 'angulo')

angulo['NVB'] = fuzz.trapmf(
    angulo.universe, [-30, -30, -18, -12])
angulo['NB'] = fuzz.trimf(
    angulo.universe, [-16.5, -10.5, -4.5])
angulo['N'] = fuzz.trimf(angulo.universe, [-9, -4.5, 0])
angulo['ZO'] = fuzz.trimf(angulo.universe, [-3, 0, 3])
angulo['P'] = fuzz.trimf(angulo.universe, [0, 4.5, 9])
angulo['PB'] = fuzz.trimf(angulo.universe, [4.5, 10.5, 16.5])
angulo['PVB'] = fuzz.trapmf(angulo.universe, [12, 18, 30, 30])


velocidadeAngular = ctrl.Antecedent(
    np.arange(-6, 6, 0.1), 'velocidadeAngular')

velocidadeAngular['NB'] = fuzz.trapmf(
    velocidadeAngular.universe, [-6, -6, -4.2, -1.7])
velocidadeAngular['N'] = fuzz.trimf(
    velocidadeAngular.universe, [-3.6, -1.7, 0])
velocidadeAngular['ZO'] = fuzz.trimf(
    velocidadeAngular.universe, [-1.7, 0, 1.7])
velocidadeAngular['P'] = fuzz.trimf(
    velocidadeAngular.universe, [0, 1.7, 3.6])
velocidadeAngular['PB'] = fuzz.trapmf(
    velocidadeAngular.universe, [1.7, 4.2, 6, 6])


posicaoCarrinho = ctrl.Antecedent(
    np.arange(-0.4, 0.4, 0.05), 'posicaoCarrinho')
posicaoCarrinho['NBIG'] = fuzz.trapmf(
    posicaoCarrinho.universe, [-0.4, -0.4, -0.3, -0.15])
posicaoCarrinho['NEG'] = fuzz.trimf(
    posicaoCarrinho.universe, [-0.3, -0.15, 0])
posicaoCarrinho['Z'] = fuzz.trimf(
    posicaoCarrinho.universe, [-0.15, 0, 0.15])
posicaoCarrinho['POS'] = fuzz.trimf(
    posicaoCarrinho.universe, [0, 0.15, 0.3])
posicaoCarrinho['PBIG'] = fuzz.trapmf(
    posicaoCarrinho.universe, [0.15, 0.3, 0.4, 0.4])


velocidadeCarrinho = ctrl.Antecedent(
    np.arange(-1, 1, 0.1), 'velocidadeCarrinho')

velocidadeCarrinho['NEG'] = fuzz.trapmf(
    velocidadeCarrinho.universe, [-1, -1, -0.1, 0])
velocidadeCarrinho['ZERO'] = fuzz.trimf(
    velocidadeCarrinho.universe, [-0.1, 0, 0.1])
velocidadeCarrinho['POS'] = fuzz.trapmf(
    velocidadeCarrinho.universe, [0, 0.1, 1, 1])

# variáveis de saída 
forcaAplicada = ctrl.Consequent(np.arange(-6, 6, 0.2), 'forcaAplicada')
forcaAplicada['NVVB'] = fuzz.trapmf(
    forcaAplicada.universe, [-6, -6, -4.8, -3.6])
forcaAplicada['NVB'] = fuzz.trimf(
    forcaAplicada.universe, [-4.8, -3.6, -2.4])
forcaAplicada['NB'] = fuzz.trimf(
    forcaAplicada.universe, [-3.6, -2.4, -1.2])
forcaAplicada['N'] = fuzz.trimf(
    forcaAplicada.universe, [-2.4, -1.2, 0])
forcaAplicada['Z'] = fuzz.trimf(
    forcaAplicada.universe, [-1.2, 0, 1.2])
forcaAplicada['P'] = fuzz.trimf(
    forcaAplicada.universe, [0, 1.2, 2.4])
forcaAplicada['PB'] = fuzz.trimf(
    forcaAplicada.universe, [1.2, 2.4, 3.6])
forcaAplicada['PVB'] = fuzz.trimf(
    forcaAplicada.universe, [2.4, 3.6, 4.8])
forcaAplicada['PVVB'] = fuzz.trapmf(
    forcaAplicada.universe, [3.6, 4.8, 6, 6])

regras = [
    ctrl.Rule(posicaoCarrinho['NBIG'] &
          velocidadeCarrinho['NEG'], forcaAplicada['PVVB']),
    ctrl.Rule(posicaoCarrinho['NEG'] &
          velocidadeCarrinho['NEG'], forcaAplicada['PVB']),
    ctrl.Rule(
        posicaoCarrinho['Z'] & velocidadeCarrinho['NEG'], forcaAplicada['PB']),
    ctrl.Rule(
        posicaoCarrinho['Z'] & velocidadeCarrinho['ZERO'], forcaAplicada['Z']),
    ctrl.Rule(
        posicaoCarrinho['Z'] & velocidadeCarrinho['POS'], forcaAplicada['NB']),
    ctrl.Rule(posicaoCarrinho['POS'] &
          velocidadeCarrinho['POS'], forcaAplicada['NVB']),
    ctrl.Rule(posicaoCarrinho['PBIG'] &
          velocidadeCarrinho['POS'], forcaAplicada['NVVB']),

    ctrl.Rule(angulo['NVB'] & velocidadeAngular['NB'],
          forcaAplicada['NVVB']),
    ctrl.Rule(angulo['NVB'] & velocidadeAngular['N'],
          forcaAplicada['NVVB']),
    ctrl.Rule(angulo['NVB'] & velocidadeAngular['ZO'],
          forcaAplicada['NVB']),
    ctrl.Rule(angulo['NVB'] & velocidadeAngular['P'],
          forcaAplicada['NB']),
    ctrl.Rule(angulo['NVB'] & velocidadeAngular['PB'],
          forcaAplicada['N']),

    ctrl.Rule(angulo['NB'] & velocidadeAngular['NB'],
          forcaAplicada['NVVB']),
    ctrl.Rule(angulo['NB'] & velocidadeAngular['N'],
          forcaAplicada['NVB']),
    ctrl.Rule(angulo['NB'] & velocidadeAngular['ZO'],
          forcaAplicada['NB']),
    ctrl.Rule(angulo['NB'] & velocidadeAngular['P'],
          forcaAplicada['N']),
    ctrl.Rule(angulo['NB'] & velocidadeAngular['PB'],
          forcaAplicada['Z']),

    ctrl.Rule(angulo['N'] & velocidadeAngular['NB'],
          forcaAplicada['NVB']),
    ctrl.Rule(angulo['N'] & velocidadeAngular['N'],
          forcaAplicada['NB']),
    ctrl.Rule(angulo['N'] & velocidadeAngular['ZO'],
          forcaAplicada['N']),
    ctrl.Rule(angulo['N'] & velocidadeAngular['P'],
          forcaAplicada['Z']),
    ctrl.Rule(angulo['N'] & velocidadeAngular['PB'],
          forcaAplicada['P']),

    ctrl.Rule(angulo['ZO'] & velocidadeAngular['NB'],
          forcaAplicada['NB']),
    ctrl.Rule(angulo['ZO'] & velocidadeAngular['N'],
          forcaAplicada['N']),
    ctrl.Rule(angulo['ZO'] & velocidadeAngular['ZO'],
          forcaAplicada['Z']),
    ctrl.Rule(angulo['ZO'] & velocidadeAngular['P'],
          forcaAplicada['P']),
    ctrl.Rule(angulo['ZO'] & velocidadeAngular['PB'],
          forcaAplicada['PB']),

    ctrl.Rule(angulo['P'] & velocidadeAngular['NB'],
          forcaAplicada['N']),
    ctrl.Rule(angulo['P'] & velocidadeAngular['N'],
          forcaAplicada['Z']),
    ctrl.Rule(angulo['P'] & velocidadeAngular['ZO'],
          forcaAplicada['P']),
    ctrl.Rule(angulo['P'] & velocidadeAngular['P'],
          forcaAplicada['PB']),
    ctrl.Rule(angulo['P'] & velocidadeAngular['PB'],
          forcaAplicada['PVB']),

    ctrl.Rule(angulo['PB'] & velocidadeAngular['NB'],
          forcaAplicada['Z']),
    ctrl.Rule(angulo['PB'] & velocidadeAngular['N'],
          forcaAplicada['P']),
    ctrl.Rule(angulo['PB'] & velocidadeAngular['ZO'],
          forcaAplicada['PB']),
    ctrl.Rule(angulo['PB'] & velocidadeAngular['P'],
          forcaAplicada['PVB']),
    ctrl.Rule(angulo['PB'] & velocidadeAngular['PB'],
          forcaAplicada['PVVB']),

    ctrl.Rule(angulo['PVB'] & velocidadeAngular['NB'],
          forcaAplicada['P']),
    ctrl.Rule(angulo['PVB'] & velocidadeAngular['N'],
          forcaAplicada['PB']),
    ctrl.Rule(angulo['PVB'] & velocidadeAngular['ZO'],
          forcaAplicada['PVB']),
    ctrl.Rule(angulo['PVB'] & velocidadeAngular['P'],
          forcaAplicada['PVVB']),
    ctrl.Rule(angulo['PVB'] & velocidadeAngular['PB'],
          forcaAplicada['PVVB'])
]

def plotarSaidasEstabilizar():
    angulo.view()
    velocidadeAngular.view()
    posicaoCarrinho.view()
    velocidadeCarrinho.view()
    forcaAplicada.view()
    plt.show()


def calculaForcaEstabilizar(valores):
    input_angulo,  input_velocidadeAngular, input_posicaoCarrinho, input_velocidadeCarrinho = valores
    calculoForca = ctrl.ControlSystemSimulation(ctrl.ControlSystem(regras))

    calculoForca.input['angulo'] = input_angulo
    calculoForca.input['velocidadeAngular'] = input_velocidadeAngular
    calculoForca.input['posicaoCarrinho'] = input_posicaoCarrinho
    calculoForca.input['velocidadeCarrinho'] = input_velocidadeCarrinho
    calculoForca.compute()

    forcaAplicadaResultado = calculoForca.output['forcaAplicada']

    plotarSimulacao(calculoForca)

    return forcaAplicadaResultado


def plotarSimulacao(calculoForca):
    angulo.view(calculoForca)
    velocidadeAngular.view(calculoForca)
    posicaoCarrinho.view(calculoForca)
    velocidadeCarrinho.view(calculoForca)
    forcaAplicada.view(calculoForca)

    plt.show()
